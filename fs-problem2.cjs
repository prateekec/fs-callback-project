const fs=require('fs');

function upperAndStore(file_path,cb){
    fs.readFile(file_path,'utf-8',(err,data)=>{
        if(err){
            return cb(err);
        }else{
            data=data.toUpperCase();
            fs.writeFile('./upperAndStore.json',data,(err)=>{
                if(err){
                    cb(err);
                }else{
                    fs.appendFile('./filename.txt','upperAndStore.json',(err) =>{
                        if(err){
                            cb(err);
                        }else{
                            cb(null,'./upperAndStore.json');
                        }
                    });
                }
            });
        }
    });
}




function lowerAndSplitStore(file_path,cb){
    fs.readFile(file_path,'utf-8',(err,data)=>{
        if(err){
            return cb(err);
        }else{
            data=data.toLowerCase();
            const sentencePattern = /(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?|\!)\s/g;
            data=data.split(sentencePattern);
            data=JSON.stringify(data,null,2);
            fs.writeFile('./lowerAndSplitStore.json',data,(err)=>{
                if(err){
                    cb(err);
                }else{
                    fs.appendFile('./filename.txt','\nlowerAndSplitStore.json',(err) =>{
                        if(err){
                            cb(err);
                        }else{

                            cb(null,'./lowerAndSplitStore.json');
                        }
                    });
                }
            });
        }
    });
}


function sortFileAndStore(file_path,cb){
    fs.readFile(file_path,'utf-8',(err,data)=>{
        if(err){
            return cb(err);
        }else{
            data=JSON.parse(data);
            data=data.sort();
            data=JSON.stringify(data,null,2);
            fs.writeFile('./sortFileAndStore.json',data,(err)=>{
                if(err){
                    cb(err);
                }else{
                    fs.appendFile('./filename.txt','\nsortFileAndStore.json',(err) =>{
                        if(err){
                            cb(err);
                        }else{
                            cb(null,'./sortFileAndStore.json');
                        }
                    });
                }
            });
        }
    });
}

function removeFiles(cb){
    fs.readFile('./filename.txt','utf-8',(err,data)=>{
        if(err){
            cb(err);
        }else{
            data=data.split('\n')
            data.forEach((filename) => {
                filename='./'+filename;
                console.log(filename);
                
                fs.rm(filename,(err)=>{
                    if(err){
                        cb(err);
                    }else{
                        console.log("File :"+filename+" deleted.");
                    }
                })
                
            });
        }
    });
}


function fsProblem2(file_path){
    upperAndStore(file_path,(err,path) =>{
        if(err){
            console.error(err);
        }else{
            lowerAndSplitStore(path,(err,path) =>{
                if(err){
                    console.error(err);
                }else{
                    sortFileAndStore(path,(err,data) =>{
                        if(err){
                            console.error(err);
                        }else{
                            removeFiles((err)=>{
                                if(err){
                                    console.error(err);
                                }
                            })
                        }
                    });
                }
            });
        }
    });    
}

module.exports=fsProblem2;
