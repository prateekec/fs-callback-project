const fs = require('fs');



function deleteFiles(dir_path,no_files,cb){
    let count=0;
    for(let file=1; file<=no_files; file++){
        let file_path=(file)+'.'+'json';
        file_path=dir_path+'/'+file;
        
        fs.unlink(file_path,(err)=>{
            if(err){
                console.error(err)
            }
            else{
                console.log("File :",file,"deleted.");
                count++;
                if(no_files==count){
                    cb(null);
                }
            }
        });
    }
}

function creatfiles(dir_path,no_files,cb){
    let count=0;
    for(let file=1; file<=no_files; file++){
        console.log(file);
        let file_path=file+'.'+'json';
        file_path=dir_path+'/'+file;
        
        fs.writeFile(file_path,'',(err)=>{
            if(err){
                console.error(err)
            }
            else{
                console.log("File :",file,"created.");
                count++;
                if(count==no_files){
                    cb(null);
                }
            }
        });
    }
}


function fsProblem1(dir_path,no_files){
    fs.mkdir(dir_path,(err) =>{
        if(err){
            console.error(err);
        }else{
            console.log("Dir is created successfully");
            creatfiles(dir_path,no_files,( err)=>{
                if(err){
                    console.error(err);
                }
                else{
                    deleteFiles(dir_path,no_files,(err)=>{
                        if(err){
                            console.error(err);
                        }else{
                            fs.rmdir(dir_path,(err)=>{
                                if(err){
                                    console.error(err);
                                }else{
                                    console.log("Folder deleted successfully.")
                                }
                            })
                        }
                    })
                }

            });
        }
    })
}

module.exports=fsProblem1;